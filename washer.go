// Package main contains the implementation of the Washer tool.
package main

import (
	"context"
	_ "embed"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/biffen/washer/internal/command"
	"gitlab.com/biffen/washer/internal/errors"
)

//go:embed docs/usage.txt
var usage string

// Main runs the whole Washer tool and returns its exit code.
func Main(ctx context.Context) (exit int) {
	var err error

	defer func() {
		if r := recover(); r != nil {
			switch t := r.(type) {
			case error:
				err = t
			default:
				//nolint:err113 // Nah.
				err = fmt.Errorf("PANIC: %v", t)
			}
		}

		exit = errors.ExitCode(err)
	}()

	var cmd *command.RootCommand
	if cmd, err = command.NewRootCommand(ctx, os.Args[1:]...); err == nil {
		if cmd.Help {
			if _, err = fmt.Fprintln(os.Stdout, usage); err != nil {
				err = fmt.Errorf("failed to print usage: %w", err)
			}
		} else {
			err = cmd.Run(ctx)
		}
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	return
}

func main() {
	var (
		ctx, stop = signal.NotifyContext(
			context.Background(),
			os.Interrupt,
			syscall.SIGTERM,
		)
		exit int
	)

	defer func() {
		os.Exit(exit)
	}()

	defer stop()

	exit = Main(ctx)
}
