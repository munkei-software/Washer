################################
Washer Documentation (|version|)
################################

Introduction
============

Washer is a tool to run checks on a tree of files, e.g. a repository.

It can be used to run linters, formatters, static analysis, etc. on a whole
project, both manually and automatically, e.g. by Git hooks or in `CI`_.

.. WARNING::

   Washer is still in early development. It might work, it might destroy your
   code! Only use it on code under version control.

.. _CI: https://en.wikipedia.org/wiki/Continuous_integration

.. toctree::
   :caption: Contents:
   :glob:
   :maxdepth: 2

   installation
   usage
   specification
   patterns
   locations
   recipes
