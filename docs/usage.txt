NAME
       washer - Linter runner

SYNOPSIS
       washer [OPTIONS] [<check|fix> [FILE…]]
       washer [OPTIONS] dump [files|specification|tools]
       washer <--help|-h>

DESCRIPTION
       Washer is a tool to run checks on a tree of files, e.g. a repository.

       It can be used to run linters, formatters, static analysis, etc. on a
       whole project, both manually and automatically, e.g. by Git hooks or in
       CI.

OPTIONS
       -C, -directory
              Run in a different directory.

       -h, --help
              Print (this) help.

       -s, --spec, --specification LOCATION
              Read specification from LOCATION instead of looking for it in the
              current directory. See /Locations/ below.

       -v, --verbose
              Print more. Can be repeated.

COMMANDS
   check [OPTIONS] [FILE…]
       Run tools to check all, or the specified, files.

       --git WHAT
              Check files in a Git repository.

              ┌─────────┬─────────────────────────────────────────────────────┐
              │ What    │ Description                                         │
              ╞═════════╪═════════════════════════════════════════════════════╡
              │ all     │ Check all files in the repository, except ignored   │
              ├─────────┼─────────────────────────────────────────────────────┤
              │ changed │ Check all files that differ from HEAD, staged or    │
              │         │ not                                                 │
              ├─────────┼─────────────────────────────────────────────────────┤
              │ tracked │ Check all tracked files                             │
              ├─────────┼─────────────────────────────────────────────────────┤
              │ staged  │ Check staged files. Backup each file and revert any │
              │         │ un-staged changes first, and restore them           │
              │         │ afterwards, so that un-staged hunks in otherwise    │
              │         │ staged files aren’t checked                         │
              │         │                                                     │
              │         │ Suitable for a Git pre-commit hook                  │
              └─────────┴─────────────────────────────────────────────────────┘

       -j, --jobs N
              The maximum number of parallel tools to run. 0, the default, means
              the same as the number of CPU cores.

       -o, --output LOCATION[=FORMAT]
              Wither to print and optionally the format of the output. See
              LOCATIONS below. Repeatable for multiple outputs. The default is
              to print in text format to STDOUT.

              ┌────────────────┬───────────────────────────────┐
              │ Format         │ Description                   │
              ╞════════════════╪═══════════════════════════════╡
              │ text (default) │ Text for humans, not parsable │
              ├────────────────┼───────────────────────────────┤
              │ codeclimate    │ Code Climate JSON             │
              ├────────────────┼───────────────────────────────┤
              │ junit          │ JUnit-like XML                │
              ├────────────────┼───────────────────────────────┤
              │ none           │ No output                     │
              └────────────────┴───────────────────────────────┘

       -p, --progress PROGRESS
              How to show progress. One of: bar, count (default), none

       -t, --tool TOOL
              Specify one or more (by repeating the option) tools to run. The
              default is to run all (applicable) tools.

       -T, --no-tool TOOL
              Don’t run a tool.

   dump [OPTIONS] [DUMP]
       Print (to STDOUT) some information about Washer.

       -z, --null-terminated
              Print U+0000-separated strings instead of lines.

       -o, --output LOCATION
              Wither to print the output. See LOCATIONS below.

       --sort
              Sort the output.

       dump files [FILE…]
              Dump all (or the specified) files that Washer would consider.

       dump spec[ification] [OPTIONS]
              Dump the specification after all normalisation.

              -f, --file
                     Print the path of the specification file, not the contents.

       dump tools [FILE…]
              Dump the names of all tools, or those that would check the listed
              files.

   fix [OPTIONS] [FILE…]
       Run tools to fix all, or the specified, files.

       Takes the same options as the check command, except --output.

LOCATIONS
       When specifying locations, for input and output, the following formats
       can be used:

       ┌───────────────┬────────────────────────────────────┐
       │ Location      │ Description                        │
       ╞═══════════════╪════════════════════════════════════╡
       │ -             │ Read from STDIN or write to STDOUT │
       ├───────────────┼────────────────────────────────────┤
       │ <PATH>        │ Read from or write to a local file │
       │ file://<PATH> │                                    │
       ├───────────────┼────────────────────────────────────┤
       │ http://<…>    │ GET from an HTTP URL               │
       │ https://<…>   │                                    │
       └───────────────┴────────────────────────────────────┘

EXIT STATUS
       0      Everything went well.

       1      Something unspecified went wrong.

       2      Usage error.

       3      A tool failed.

       4      Washer was cancelled.

ENVIRONMENT
       PS4
              Used as prefix when printing commands before they are executed.

       WASHER_DEBUG
              If not empty then Washer will print a lot of debugging
              information.

       WASHER_NO_TOOL
              A comma-separated list of tools to not run.

       WASHER_PROGRESS
              Default progress if --progress is not specified.

       WASHER_SPECIFICATION
              Use some other specification than the default.

BUGS
       https://gitlab.com/biffen/washer/-/issues

COPYRIGHT
       Copyright © 2020, 2021, 2022 Theo Willows

       This program is free software: you can redistribute it and/or modify it
       under the terms of the GNU General Public License as published by the
       Free Software Foundation, either version 3 of the License, or (at your
       option) any later version.

       This program is distributed in the hope that it will be useful, but
       WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
       Public License for more details.

       You should have received a copy of the GNU General Public License along
       with this program. If not, see <https://www.gnu.org/licenses/>.
