.. _locations:

*********
Locations
*********

A location is a string that specifies the location of some data. It can be a
path or a `URL`_.

Paths as well as URLs with a ``file:`` scheme are local files.

URLs with an ``http`` or ``https`` scheme are data read or written over HTTP.

.. _URL: https://en.wikipedia.org/wiki/URL
