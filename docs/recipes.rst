*******
Recipes
*******

.. highlight:: yaml

Here are some examples of how to use Washer. Like food recipes they can be
modified to taste.

Executable
==========

Checks (and fixes) that all files with a `shebang`_ are executable, and those
without one are not.

.. literalinclude:: recipes/executable.yaml

.. _shebang: https://en.wikipedia.org/wiki/Shebang_(Unix)

Go
==

Runs the following linters and formatters on all `Go`_ files:

- `gofmt`_
- `gofumpt`_
- `goimports`_
- `golangci-lint`_
- `gosrt`_
- `govet`_

Some of the above don’t work well with a single file, so they use ``global:
true``. Others don’t exit non-zero, so they use ``output: empty``.

.. literalinclude:: recipes/go.yaml

.. _Go: https://go.dev
.. _gofmt: https://pkg.go.dev/cmd/gofmt
.. _gofumpt: https://github.com/mvdan/gofumpt
.. _goimports: https://pkg.go.dev/golang.org/x/tools/cmd/goimports
.. _golangci-lint: https://golangci-lint.run
.. _gosrt: https://gitlab.com/biffen/gosrt
.. _govet: https://pkg.go.dev/cmd/vet

Hadolint
========

Runs `Hadolint`_ on Dockerfiles.

.. literalinclude:: recipes/hadolint.yaml

.. _Hadolint: https://github.com/hadolint/hadolint

Markdown
========

.. literalinclude:: recipes/markdown.yaml

Perl
====

.. literalinclude:: recipes/perl.yaml

.. NOTE::

   The the YAML anchor to avoid repeating the include patterns.

Prettier
========

.. literalinclude:: recipes/prettier.yaml

Python
======

Runs `Black`_, `isort`_, `Pylint`_ and `Mypy`_ on all `Python`_ files.

.. literalinclude:: recipes/python.yaml

.. _Black: https://github.com/psf/black
.. _Mypy: https://mypy.readthedocs.io/en/stable/
.. _Pylint: https://www.pylint.org
.. _Python: https://www.python.org
.. _isort: https://github.com/PyCQA/isort

.. NOTE::

   The the YAML anchor to avoid repeating the include patterns.

ShellCheck
==========

Runs `ShellCheck`_ on files that look like shell scripts.

.. literalinclude:: recipes/shellcheck.yaml

.. _ShellCheck: https://www.shellcheck.net

Text
====

Runs `textlint`_ on `Markdown`_, `reStructuredText`_ and pure text files.

.. literalinclude:: recipes/text.yaml

.. _textlint: https://textlint.github.io
.. _Markdown: https://daringfireball.net/projects/markdown/
.. _reStructuredText: https://docutils.sourceforge.io/rst.html

YAML
====

.. literalinclude:: recipes/yaml.yaml

Git Hook
========

Here is an example of how to run Washer as a ``pre-commit`` `Git hook`_:

.. code-block:: sh
   :caption: ``pre-commit``

   #!/bin/sh

   set -e

   if washer dump spec -f >/dev/null 2>&1
   then
     (
       set -x
       washer check --git staged
     )
   fi

.. _Git hook: https://git-scm.com/docs/githooks
