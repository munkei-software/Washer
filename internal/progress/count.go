package progress

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
)

var _ Progress = (*Count)(nil)

const ProgressCount = "count"

const S = "⠉⠁" + "⠈⠉" + " ⠙" + " ⠸" + " ⢰" + " ⣠" +
	"⢀⣀" + "⣀⡀" + "⣄ " + "⡆ " + "⠇ " + "⠋ "

type Count struct {
	Config

	mtx                 sync.Mutex
	done, total, failed int
}

func NewCount(config Config) *Count {
	return &Count{
		Config: config,
	}
}

func (c *Count) AfterTask(_ context.Context, res *task.Result) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	c.done++

	if !res.Success {
		c.failed++
	}

	return c.update()
}

func (c *Count) BeforeTask(context.Context, *spec.Tool, *file.File) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	c.total++

	return c.update()
}

func (c *Count) Close() error {
	_, err := fmt.Fprintln(c.Writer, "")

	return err
}

func (c *Count) Open(context.Context) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	return c.update()
}

func (c *Count) update() (err error) {
	var (
		i       = (c.done * (1 + 1)) % len([]rune(S))
		spinner = string([]rune(S)[i : i+2])
	)

	str := fmt.Sprintf("washer: %s Total: %d", spinner, c.total)

	if c.done > 0 {
		str += "; " +
			c.Aurora.Green(
				fmt.Sprintf("good: %d", c.done-c.failed),
			).Bold().String()

		if c.failed > 0 {
			str += "; " +
				c.Aurora.Red(
					fmt.Sprintf("failed: %d", c.failed),
				).Bold().String()
		}
	}

	_, err = fmt.Fprint(
		c.Writer,
		str,
		"\r",
	)

	return
}
