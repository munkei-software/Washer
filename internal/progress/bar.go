package progress

import (
	"context"
	"fmt"
	"math"
	"os"
	"sync"

	"github.com/logrusorgru/aurora/v3"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
	"golang.org/x/term"
)

var _ Progress = (*Bar)(nil)

const (
	BASE        = 0x2588
	EIGHT       = 8
	ProgressBar = "bar"
	WIDTH       = 80
)

type Bar struct {
	Config

	width       int
	mtx         sync.Mutex
	done, total int
	failed      bool
}

func NewBar(config Config) *Bar {
	b := &Bar{
		Config: config,
		width:  WIDTH,
	}

	if f, ok := b.Writer.(*os.File); ok {
		if width, _, err := term.GetSize(int(f.Fd())); err == nil &&
			width > 0 {
			b.width = width
		}
	}

	return b
}

func (b *Bar) AfterTask(_ context.Context, res *task.Result) error {
	b.mtx.Lock()
	defer b.mtx.Unlock()

	b.done++

	if !res.Success {
		b.failed = true
	}

	return b.update()
}

func (b *Bar) BeforeTask(context.Context, *spec.Tool, *file.File) error {
	b.mtx.Lock()
	defer b.mtx.Unlock()

	b.total++

	return b.update()
}

func (b *Bar) Close() (err error) {
	_, err = fmt.Fprintln(b.Writer, "")

	return
}

func (b *Bar) Open(context.Context) error {
	return nil
}

func (b *Bar) bar(width int) string {
	var (
		bar           = make([]rune, width)
		i             int
		integer, frac = math.Modf(
			(float64(b.done) / float64(b.total)) *
				float64(width),
		)
	)

	for i < int(integer) {
		bar[i] = '█'
		i++
	}

	if frac != 0 {
		bar[i] = rune(BASE + (EIGHT - frac*EIGHT))
		i++
	}

	for i < len(bar) {
		bar[i] = ' '
		i++
	}

	return string(bar)
}

func (b *Bar) update() error {
	str := fmt.Sprintf("%d/%d ", b.done, b.total)
	bar := b.bar(b.width - len(str))

	var v aurora.Value

	switch {
	case b.failed:
		v = b.Aurora.Red(bar).BgBlue()
	case b.done >= b.total:
		v = b.Aurora.Green(bar)
	default:
		v = b.Aurora.Cyan(bar).BgBlue()
	}

	_, err := fmt.Fprintf(b.Writer, "%s%s\r", str, v)

	return err
}
