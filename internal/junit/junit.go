package junit

import (
	"encoding/xml"
	"fmt"
	"io"
	"time"
)

// Error indicates that the test errored. An errored test had an unanticipated
// problem. For example an unchecked throwable (exception), crash or a problem
// with the implementation of the test. Contains as a text node relevant data
// for the error, for example a stack trace.
//
// Optional.
type Error struct {
	XMLName xml.Name `xml:"error"`

	// Message is the error message. e.g., if a java exception is thrown, the
	// return value of getMessage()
	Message string `xml:"message,attr,omitempty"`

	// The type of error that occurred. e.g., if a java exception is thrown the
	// full class name of the exception.
	Type string `xml:"type,attr,omitempty"`

	Description string
}

// Failure indicates that the test failed. A failure is a condition which the
// code has explicitly failed by using the mechanisms for that purpose. For
// example via an assertEquals. Contains as a text node relevant data for the
// failure, e.g., a stack trace.
//
// Optional.
type Failure struct {
	XMLName xml.Name `xml:"failure"`

	// Message is the message specified in the assert.
	Message string `xml:"message,attr,omitempty"`

	// The type of the assert.
	Type string `xml:"type,attr,omitempty"`

	Description string `xml:",chardata"`
}

// TestCase can appear multiple times, see /testsuites/testsuite@tests.
type TestCase struct {
	XMLName xml.Name `xml:"testcase"`

	// File is a non-standard attribute used by, at least, GitLab.
	File string `xml:"file,attr,omitempty"`

	// Name of the test method, required.
	Name string `xml:"name,attr"`

	// Assertions is number of assertions in the test case.
	//
	// Optional.
	Assertions uint64 `xml:"assertions,attr,omitempty"`

	// Classname is the full class name for the class the test method is in.
	//
	// Required.
	Classname string `xml:"classname,attr,omitempty"`

	Status string `xml:"status,attr,omitempty"`

	// Time taken (in seconds) to execute the test.
	//
	// Optional.
	Time *Duration `xml:"time,attr,omitempty"`

	// Skipped can appear 0 or once.
	//
	// If the test was not executed or failed, you can specify one of the
	// skipped, error or failure elements.
	Skipped *struct {
		XMLName xml.Name `xml:"skipped"`

		// Message/description string why the test case was skipped.
		//
		// Optional.
		Message string `xml:"message,attr,omitempty"`
	}

	Error *Error

	Failure *Failure

	// Data that was written to standard out while the test was executed.
	//
	// Optional.
	SystemOut []byte `xml:"system-out,omitempty"`

	// Data that was written to standard error while the test was executed.
	//
	// Optional.
	SystemErr []byte `xml:"system-err,omitempty"`
}

// TestSuite can appear multiple times, if contained in a testsuites element. It
// can also be the root element.
type TestSuite struct {
	XMLName xml.Name `xml:"testsuite"`

	// Name is the full (class) name of the test for non-aggregated testsuite
	// documents.
	Name string `xml:"name,attr"`

	// Tests is the total number of tests in the suite, required.
	Tests uint64 `xml:"tests,attr"`

	// Disabled is the total number of disabled tests in the suite.
	//
	// Optional.
	Disabled uint64 `xml:"disabled,attr,omitempty"`

	// Errors is the total number of tests in the suite that errored. An errored
	// test is one that had an unanticipated problem,
	Errors uint64 `xml:"errors,attr"`

	// For is the example an unchecked throwable; or a problem with the
	// implementation of the test.
	//
	// Optional.
	For string `xml:"for,attr,omitempty"`

	// Failures is the total number of tests in the suite that failed. A failure
	// is a test which the code has explicitly failed
	Failures uint64 `xml:"failures,attr"`

	// By using the mechanisms for that purpose. e.g., via an assertEquals.
	//
	// Optional.
	By string `xml:"by,attr,omitempty"`

	// Hostname is the host on which the tests were executed. 'localhost' should
	// be used if the hostname cannot be determined.
	//
	// Optional.
	Hostname string `xml:"hostname,attr,omitempty"`

	// Id starts at 0 for the first testsuite and is incremented by 1 for each
	// following testsuite
	ID uint64 `xml:"id,attr"`

	// Package is derived from testsuite/@name in the non-aggregated documents.
	//
	// Optional.
	Package string `xml:"package,attr,omitempty"`

	// Skipped is the total number of skipped tests.
	//
	// Optional.
	Skipped uint64 `xml:"skipped,attr,omitempty"`

	// Time taken (in seconds) to execute the tests in the suite.
	//
	// Optional.
	Time *Duration `xml:"time,attr,omitempty"`

	// Timestamp when the test was executed in ISO 8601 format
	// (2014-01-21T16:17:18). Timezone may not be specified.
	//
	// Optional.
	Timestamp *time.Time `xml:"timestamp,attr,omitempty"`

	// Properties (e.g., environment settings) set during test execution. The
	// properties element can appear 0 or once.
	Properties *struct {
		XMLName xml.Name `xml:"properties"`

		// Property can appear multiple times. The name and value attributres
		// are required.
		Property []struct {
			XMLName xml.Name `xml:"property"`

			Name  string `xml:"name,attr"`
			Value string `xml:"value,attr"`
		} `xml:"property"`
	}

	TestCases []*TestCase `xml:"testcase"`

	// Data that was written to standard out while the test suite was executed.
	//
	// Optional.
	SystemOut []byte `xml:"system-out,omitempty"`

	// Data that was written to standard error while the test suite was
	// executed.
	//
	// Optional.
	SystemErr []byte `xml:"system-err,omitempty"`
}

func (suite *TestSuite) calculate() {
	suite.Disabled = 0
	suite.Errors = 0
	suite.Failures = 0
	suite.Skipped = 0
	suite.Tests = uint64(len(suite.TestCases))

	for _, testCase := range suite.TestCases {
		if testCase.Error != nil {
			suite.Errors++
		}

		if testCase.Failure != nil {
			suite.Failures++
		}

		if testCase.Skipped != nil {
			suite.Skipped++
		}
	}
}

type TestSuites struct {
	XMLName xml.Name `xml:"testsuites"`

	// Disabled is the total number of disabled tests from all testsuites.
	Disabled uint64 `xml:"disabled,attr"`

	// Errors is the total number of tests with error result from all
	// testsuites.
	Errors uint64 `xml:"errors,attr"`

	// Failures is the total number of failed tests from all testsuites.
	Failures uint64 `xml:"failures,attr"`

	// Name is the collective name of the suites.
	Name string `xml:"name,attr"`

	// Tests is the total number of tests from all testsuites. Some software may
	// expect to only see the number of successful tests from all testsuites
	// though.
	Tests uint64 `xml:"tests,attr"`

	// Time in seconds to execute all test suites.
	Time *Duration `xml:"time,attr,omitempty"`

	TestSuites []*TestSuite `xml:"testsuite"`
}

func (suites *TestSuites) Print(w io.Writer) error {
	if _, err := fmt.Fprint(w, xml.Header); err != nil {
		return fmt.Errorf("failed to print JUnit XML: %w", err)
	}

	suites.calculate()

	enc := xml.NewEncoder(w)
	enc.Indent("", "  ")

	if err := enc.Encode(suites); err != nil {
		return fmt.Errorf("failed to encode JUnit XML: %w", err)
	}

	return nil
}

func (suites *TestSuites) Suite(name string, now time.Time) *TestSuite {
	for _, suite := range suites.TestSuites {
		if suite.Name == name {
			return suite
		}
	}

	suite := &TestSuite{
		ID:        uint64(len(suites.TestSuites)),
		Name:      name,
		Timestamp: &now,
	}
	suites.TestSuites = append(suites.TestSuites, suite)

	return suite
}

func (suites *TestSuites) calculate() {
	suites.Disabled = 0
	suites.Errors = 0
	suites.Failures = 0
	suites.Tests = 0

	for _, suite := range suites.TestSuites {
		suite.calculate()

		suites.Disabled += suite.Disabled
		suites.Errors += suite.Errors
		suites.Failures += suite.Failures
		suites.Tests += suite.Tests
	}
}
