package junit

import (
	"time"
)

type Duration float64

func NewDuration(d *time.Duration) *Duration {
	s := d.Seconds()

	return (*Duration)(&s)
}
