package util

import "context"

type OrderedSet[T Element[T]] struct {
	set[T]
}

func NewOrderedSet[T Element[T]](
	ctx context.Context,
	ts ...T,
) *OrderedSet[T] {
	s := new(OrderedSet[T])
	s.Insert(ctx, ts...)

	return s
}

func (s *OrderedSet[T]) Contains(ctx context.Context, t T) (ok bool) {
	if s == nil {
		return
	}

	ok, _ = Any[T](ctx, s, func(e T) bool {
		return ElementsEqual(e, t)
	})

	return
}

func (s *OrderedSet[T]) Insert(
	ctx context.Context,
	ts ...T,
) (inserted int) {
	for _, t := range ts {
		if s.Contains(ctx, t) {
			continue
		}

		s.slice = append(s.slice, t)
		inserted++
	}

	return
}
