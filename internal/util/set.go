package util

import (
	"context"
)

type set[T Element[T]] struct {
	slice []T
}

func (s *set[T]) Each(ctx context.Context, f func(T) bool) error {
OUTER:
	for _, t := range s.slice {
		select {
		case <-ctx.Done():
			return ctx.Err()

		default:
			if !f(t) {
				break OUTER
			}
		}
	}

	return nil
}

func (s set[T]) Empty() bool {
	return s.Len() == 0
}

func (s set[T]) Len() int {
	return len(s.slice)
}

func (s *set[T]) RemoveIf(
	_ context.Context,
	f func(T) bool,
) (removed int, err error) {
	var n int

	for _, t := range s.slice {
		if f(t) {
			removed++

			continue
		}

		s.slice[n] = t
		n++
	}

	s.slice = s.slice[:n]

	return
}

func (s *set[T]) Slice() []T {
	slice := make([]T, len(s.slice))
	copy(slice, s.slice)

	return slice
}
