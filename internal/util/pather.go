package util

type Pather interface {
	Path() (string, error)
}
