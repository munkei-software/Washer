package util

import (
	"context"

	"golang.org/x/sync/errgroup"
)

type Chain[T any] struct {
	Transformers []Transformer[T]
	ChanSize     int
}

func (sc *Chain[T]) Run(
	ctx context.Context,
	init ...T,
) (results <-chan T, callback func() error) {
	g, ctx := errgroup.WithContext(ctx)

	in := make(chan T, sc.ChanSize)
	func(in chan<- T, init []T) {
		g.Go(func() error {
			defer close(in)

			for _, f := range init {
				select {
				case <-ctx.Done():
					return ctx.Err()

				default:
					in <- f
				}
			}

			return nil
		})
	}(in, init)

	var out chan T

	for _, t := range sc.Transformers {
		t := t
		out = make(chan T, sc.ChanSize)

		func(ctx context.Context, in <-chan T, out chan<- T) {
			g.Go(func() error {
				defer close(out)

				return t(ctx, in, out)
			})
		}(ctx, in, out)

		in = out
	}

	return out, g.Wait
}

type Transformer[T any] func(
	ctx context.Context,
	in <-chan T,
	out chan<- T,
) error

func Exclude[T any](f func(T) bool) Transformer[T] {
	return func(ctx context.Context, in <-chan T, out chan<- T) error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()

			case t, ok := <-in:
				if !ok {
					return nil
				}

				if f(t) {
					continue
				}

				out <- t
			}
		}
	}
}
