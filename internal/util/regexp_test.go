package util_test

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/util"
)

func TestUnmarshalRegexp(t *testing.T) {
	t.Parallel()

	type E struct {
		Bool bool `regex:"bool"`
	}

	type S struct {
		E
		String string `regexp:"str"`
		Int    int    `regexp:"num"`
	}

	var (
		re = regexp.MustCompile(
			`(?i)(?P<str>[a-z]*):(?P<num>\d*) (?P<bool>true|false)`,
		)
		s S
	)

	m := re.FindStringSubmatch(`foo:42 true`)

	err := util.UnmarshalRegexp(re, m, &s)

	require.NoError(t, err)

	assert.Equal(t, S{
		E{true},
		"foo",
		42,
	}, s)
}
