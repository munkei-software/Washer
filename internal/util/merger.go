package util

import "reflect"

type Merger interface {
	Merge(src reflect.Value) error
}

type MergerTransformer struct{}

func (t MergerTransformer) Transformer(
	typ reflect.Type,
) func(dst, src reflect.Value) error {
	if typ.Implements(reflect.TypeOf((*Merger)(nil)).Elem()) {
		return func(dst, src reflect.Value) error {
			//nolint:forcetypeassert,wrapcheck // Checked above.
			return dst.Interface().(Merger).Merge(src)
		}
	}

	return nil
}
