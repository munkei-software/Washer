package util

import (
	"regexp"

	"github.com/mitchellh/mapstructure"
)

func UnmarshalRegexp(re *regexp.Regexp, match []string, v interface{}) error {
	names := re.SubexpNames()
	groups := make(map[string]string, len(names))

	for _, name := range names {
		i := re.SubexpIndex(name)
		if i <= 0 {
			continue
		}

		if match[i] == "" {
			continue
		}

		groups[name] = match[i]
	}

	d, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:           v,
		Squash:           true,
		TagName:          "regexp",
		WeaklyTypedInput: true,
	})
	if err != nil {
		return err
	}

	return d.Decode(groups)
}
