package util_test

import (
	"context"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/util"
)

func TestEach(t *testing.T) {
	t.Parallel()

	var (
		ctx = context.Background()
		s   = []int{1, 2, 3}
	)

	err := util.Each(ctx, s, func(i *int) bool {
		*i *= *i

		return true
	})
	assert.NoError(t, err)

	assert.Equal(t, []int{1, 4, 9}, s)
}

func TestTransform(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	assert.Equal(
		t,
		[]string{"FOO", "BAR", "BAZ"},
		util.Transform(ctx, strings.ToUpper, "foo", "bar", "baz"),
	)

	assert.Equal(
		t,
		[]string{"f-o-o", "b-ar", "b-az"},
		util.Transform(ctx, func(s []string) string {
			return strings.Join(s, "-")
		}, []string{"f", "o", "o"}, []string{"b", "ar"}, []string{"b", "az"}),
	)
}

func TestTransformErr(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	actual, err := util.TransformErr(ctx, func(str string) (string, error) {
		if strings.HasPrefix(str, "b") {
			//nolint:err113 // Just a test.
			return "", errors.New("b error")
		}

		return strings.ToUpper(str), nil
	}, "foo", "bar", "baz")
	assert.Error(t, err)
	assert.Equal(t, []string{"FOO"}, actual)
}
