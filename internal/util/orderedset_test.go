package util_test

import (
	"testing"

	"gitlab.com/biffen/washer/internal/util"
)

var _ util.Container[String] = (*util.OrderedSet[String])(nil)

func TestOrderedStringSet(t *testing.T) {
	t.Parallel()

	testSet(t, util.NewOrderedSet[String], false)
}
