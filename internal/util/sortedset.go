package util

import (
	"context"
	"sort"
)

type SortedSet[T Element[T]] struct {
	set[T]
}

func NewSortedSet[T Element[T]](
	ctx context.Context,
	ts ...T,
) *SortedSet[T] {
	s := new(SortedSet[T])
	s.Insert(ctx, ts...)

	return s
}

func (s *SortedSet[T]) Contains(ctx context.Context, t T) (ok bool) {
	_, ok = s.search(ctx, t)

	return
}

func (s *SortedSet[T]) Insert(
	ctx context.Context,
	ts ...T,
) (inserted int) {
	for _, file := range ts {
		if s.insert(ctx, file) {
			inserted++
		}
	}

	return
}

func (s *SortedSet[T]) IntDiff(
	ctx context.Context,
	other *SortedSet[T],
) (thisDiff, intersection, otherDiff SortedSet[T]) {
	var i, j int

	for i < len(s.slice) && j < len(other.slice) {
		var (
			a = s.slice[i]
			b = other.slice[j]
			c = a.Compare(b)
		)

		switch {
		case c < 0:
			thisDiff.insert(ctx, a)
			i++

		case c == 0:
			intersection.insert(ctx, a)
			i++
			j++

		case c > 0:
			thisDiff.insert(ctx, b)
			j++
		}
	}

	thisDiff.Insert(ctx, s.slice[i:]...)
	otherDiff.Insert(ctx, other.slice[j:]...)

	return
}

func (s *SortedSet[T]) insert(
	ctx context.Context,
	t T,
) (inserted bool) {
	var (
		i  int
		ok bool
	)

	i, ok = s.search(ctx, t)

	if ok {
		return
	}

	tmp := make([]T, 0, len(s.slice)+1)
	tmp = append(tmp, s.slice...)

	var newT T
	tmp = append(tmp, newT)
	copy(tmp[i+1:], tmp[i:])
	tmp[i] = t
	s.slice = tmp

	inserted = true

	return
}

func (s *SortedSet[T]) search(
	_ context.Context,
	t T,
) (i int, ok bool) {
	i = sort.Search(s.Len(), func(i int) bool {
		return s.slice[i].Compare(t) >= 0
	})

	ok = i < s.Len() && ElementsEqual(s.slice[i], t)

	return
}
