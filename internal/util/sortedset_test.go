package util_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/util"
)

var _ util.Container[String] = (*util.SortedSet[String])(nil)

func TestSortedSet(t *testing.T) {
	t.Parallel()

	testSet(t, util.NewSortedSet[String], true)
}

func TestSortedSet_IntDiff(t *testing.T) {
	t.Parallel()

	var (
		ctx = context.Background()
		n   = func(s []string) *util.SortedSet[String] {
			return util.NewSortedSet(
				ctx,
				util.Transform(
					ctx,
					func(str string) String { return String(str) },
					s...)...)
		}
	)

	for _, c := range [...]struct {
		A, B, ExpectedA, ExpectedInt, ExpectedB []string
	}{
		{
			A:           []string{"a", "b", "c"},
			B:           []string{"b", "c", "d"},
			ExpectedA:   []string{"a"},
			ExpectedInt: []string{"b", "c"},
			ExpectedB:   []string{"d"},
		},

		{
			A:           []string{"a", "b", "c"},
			B:           []string{"c", "d", "e"},
			ExpectedA:   []string{"a", "b"},
			ExpectedInt: []string{"c"},
			ExpectedB:   []string{"d", "e"},
		},
	} {
		c := c

		t.Run("", func(t *testing.T) {
			t.Parallel()
			a := n(c.A)
			b := n(c.B)

			ad, i, bd := a.IntDiff(ctx, b)

			assert.Equal(t, n(c.ExpectedA), &ad)
			assert.Equal(t, n(c.ExpectedInt), &i)
			assert.Equal(t, n(c.ExpectedB), &bd)
		})
	}
}
