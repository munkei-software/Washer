package data_test

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/data"
)

const (
	identifiableString = "4ef53505-a37a-40f4-b3ba-fb1447498470"
)

func TestReader(t *testing.T) {
	t.Parallel()

	var (
		ctx = context.Background()
		err error
		r   io.Reader
	)

	r, err = data.NewReader(ctx, "")
	assert.Nil(t, r)
	assert.Error(t, err)

	r, err = data.NewReader(ctx, "-")
	assert.NotNil(t, r)
	assert.NoError(t, err)

	server := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, identifiableString)
		}),
	)
	defer server.Close()

	r, err = data.NewReader(
		ctx,
		server.URL,
		data.OptionHTTPClient(server.Client()),
	)
	assert.NoError(t, err)

	if assert.NotNil(t, r) {
		b, err := io.ReadAll(r)
		assert.NoError(t, err)

		assert.Equal(t, []byte(identifiableString+"\n"), b)
	}
}
