package data

import (
	"context"
	"fmt"
	"io"
	"os"
)

func NewWriter(
	ctx context.Context,
	location string,
	options ...Option,
) (io.WriteCloser, error) {
	o, err := getOpts(options...)
	if err != nil {
		return nil, err
	}

	path, u, err := pathOrURL(location)

	switch {
	case err != nil:
		return nil, err

	case u != nil:
		switch u.Scheme {
		// case "http","https":
		// return NewHTTPWriter(ctx, u, o)
		case SchemeFile:
			return NewFileWriter(ctx, u.Path, o)

		default:
			return nil, fmt.Errorf("%w: %q", ErrUnsupportedScheme, u.Scheme)
		}

	case path == STD:
		return os.Stdout, nil

	default:
		return NewFileWriter(ctx, path, o)
	}
}
