package data

import (
	"context"
	"fmt"
	"io"
	"os"
)

func NewReader(
	ctx context.Context,
	location string,
	options ...Option,
) (io.ReadCloser, error) {
	o, err := getOpts(options...)
	if err != nil {
		return nil, err
	}

	path, u, err := pathOrURL(location)

	switch {
	case err != nil:
		return nil, err

	case u != nil:
		switch u.Scheme {
		case "http", "https":
			return NewHTTPReader(ctx, u, o)

		case SchemeFile:
			return NewFileReader(ctx, u.Path, o)

		default:
			return nil, fmt.Errorf("%w: %q", ErrUnsupportedScheme, u.Scheme)
		}

	case path == STD:
		if o.Stderr {
			return os.Stderr, nil
		}

		return os.Stdin, nil

	default:
		return NewFileReader(ctx, path, o)
	}
}
