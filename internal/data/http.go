package data

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

var _ io.ReadCloser = (*HTTPReader)(nil)

type HTTPReader struct {
	r io.ReadCloser
}

func NewHTTPReader(
	ctx context.Context,
	//nolint:interfacer // Must match signature.
	u *url.URL,
	o *opts,
) (*HTTPReader, error) {
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		u.String(),
		http.NoBody,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create HTTP request: %w", err)
	}

	client := o.HTTPClient
	if client == nil {
		client = new(http.Client)
	}

	//nolint:bodyclose // The reader is returned.
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to send HTTP request: %w", err)
	}

	return &HTTPReader{resp.Body}, nil
}

func (h *HTTPReader) Close() error {
	return h.r.Close()
}

func (h *HTTPReader) Read(p []byte) (n int, err error) {
	return h.r.Read(p)
}
