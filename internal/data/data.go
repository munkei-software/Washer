package data

import (
	"net/url"
	"strings"
)

const (
	STD                  = "-"
	ErrEmptyLocation     = Error("empty location")
	ErrUnsupportedScheme = Error("unsupported scheme")
)

func AsPath(location string) (string, error) {
	path, u, err := pathOrURL(location)
	if err != nil {
		return "", err
	}

	if u != nil && u.Scheme == SchemeFile {
		return u.Path, nil
	}

	return path, nil
}

func pathOrURL(location string) (path string, u *url.URL, err error) {
	switch {
	case location == "":
		err = ErrEmptyLocation

	//nolint:gomnd // golangci-lint doesn’t seem to allow configuration.
	case len(strings.SplitN(location, ":", 2)) == 2:
		u, err = url.Parse(location)

	default:
		path = location
	}

	return
}
