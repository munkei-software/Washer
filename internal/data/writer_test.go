package data_test

import (
	"context"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/data"
)

func TestWriter(t *testing.T) {
	t.Parallel()

	var (
		ctx = context.Background()
		err error
		w   io.Writer
	)

	w, err = data.NewWriter(ctx, "")
	assert.Nil(t, w)
	assert.Error(t, err)

	w, err = data.NewWriter(ctx, "-")
	assert.NotNil(t, w)
	assert.NoError(t, err)
}
