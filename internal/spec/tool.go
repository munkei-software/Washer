package spec

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"strings"

	"github.com/imdario/mergo"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/pattern"
	"gitlab.com/biffen/washer/internal/util"
	"gopkg.in/yaml.v3"
)

var (
	_ fmt.Stringer        = (*Tool)(nil)
	_ util.Element[*Tool] = (*Tool)(nil)
	_ util.Merger         = (*ToolSet)(nil)
	_ yaml.Marshaler      = (*ToolSet)(nil)
	_ yaml.Unmarshaler    = (*ToolSet)(nil)
)

type Tool struct {
	CheckCmd *YAMLCommand `yaml:"check,omitempty"`
	Exclude  *pattern.Set `yaml:"exclude,omitempty"`
	FixCmd   *YAMLCommand `yaml:"fix,omitempty"`
	Include  *pattern.Set `yaml:"include,omitempty"`
	Name     string       `yaml:"-"`
	Parse    string       `yaml:"parse,omitempty"`

	deleted bool
}

func (tool *Tool) Compare(other *Tool) int {
	return strings.Compare(tool.Name, other.Name)
}

func (tool *Tool) Includes(
	ctx context.Context,
	f *file.File,
) (ok bool, err error) {
	ok = true

	if tool.Include != nil {
		ok, _, err = tool.Include.Match(ctx, f)
	}

	if ok &&
		err == nil &&
		tool.Exclude != nil {
		ok, _, err = tool.Exclude.Match(ctx, f)
		ok = !ok
	}

	return
}

func (tool *Tool) String() string {
	return tool.Name
}

type ToolSet struct {
	util.SortedSet[*Tool]
}

func (ts *ToolSet) Load(
	ctx context.Context,
	name string,
) (tool *Tool, ok bool, err error) {
	err = ts.Each(ctx, func(t *Tool) bool {
		if t.Name == name {
			if !t.deleted {
				ok = true
				tool = t
			}

			return false
		}

		return true
	})

	return
}

func (ts *ToolSet) MarshalYAML() (interface{}, error) {
	tmp := make(map[string]*Tool)

	if err := ts.Each(context.Background(), func(tool *Tool) bool {
		if tool.deleted {
			tmp[tool.Name] = nil

			return true
		}

		tmp[tool.Name] = tool

		return true
	}); err != nil {
		return nil, fmt.Errorf("failed to encode tool set to YAML: %w", err)
	}

	return tmp, nil
}

func (ts *ToolSet) Merge(src reflect.Value) error {
	other, ok := src.Interface().(*ToolSet)
	if !ok {
		panic(errors.Internal(nil))
	}

	var (
		ctx      = context.Background()
		innerErr error
		slice    = ts.Slice()
	)

	if err := other.Each(ctx, func(tool *Tool) bool {
		i := sort.Search(len(slice), func(i int) bool {
			return slice[i].Name >= tool.Name
		})

		exists := i < len(slice) && slice[i].Name == tool.Name

		switch {
		case exists:
			if tool.deleted {
				slice[i] = tool
			} else if err := mergo.Merge(slice[i], tool); err != nil {
				innerErr = fmt.Errorf("failed to merge specifications: %w", err)

				return false
			}

		case i < len(slice):
			slice = append(
				slice[:i],
				append([]*Tool{tool}, slice[i:]...)...,
			)

		default:
			slice = append(slice, tool)
		}

		return true
	}); err != nil {
		return err
	}

	if innerErr != nil {
		return innerErr
	}

	*ts = ToolSet{}
	ts.Insert(ctx, slice...)

	return nil
}

func (ts *ToolSet) UnmarshalYAML(value *yaml.Node) error {
	tmp := make(map[string]*Tool)

	if err := value.Decode(tmp); err != nil {
		return err
	}

	*ts = ToolSet{}

	for name, tool := range tmp {
		if tool == nil {
			tool = &Tool{deleted: true}
		}

		tool.Name = name
		ts.Insert(context.Background(), tool)
	}

	return nil
}
