package spec

import (
	"encoding"
	"fmt"

	"gopkg.in/yaml.v3"
)

const (
	ExitIgnore  Exit = "ignore"
	ExitInverse Exit = "inverse"

	FileAppend  File = "append"
	FileDiscard File = "discard"

	OutputEmpty Output = "empty"
)

var (
	_ encoding.TextMarshaler   = (*Exit)(nil)
	_ encoding.TextMarshaler   = (*File)(nil)
	_ encoding.TextMarshaler   = (*Output)(nil)
	_ encoding.TextUnmarshaler = (*Exit)(nil)
	_ encoding.TextUnmarshaler = (*File)(nil)
	_ encoding.TextUnmarshaler = (*Output)(nil)
	_ fmt.Stringer             = (*Exit)(nil)
	_ fmt.Stringer             = (*File)(nil)
	_ fmt.Stringer             = (*Output)(nil)
	_ yaml.Unmarshaler         = (*YAMLCommand)(nil)
)

type Command struct {
	Command []string `yaml:"command"`
	Exit    Exit     `yaml:"exit,omitempty"`
	File    File     `yaml:"file,omitempty"`
	Global  bool     `yaml:"global,omitempty"`
	Output  Output   `yaml:"output,omitempty"`
}

type Exit string

func (e Exit) MarshalText() (text []byte, err error) {
	return []byte(e.String()), nil
}

func (e Exit) String() string {
	return string(e)
}

func (e *Exit) UnmarshalText(text []byte) error {
	switch Exit(text) {
	case "", ExitIgnore, ExitInverse:
		*e = Exit(text)

		return nil

	default:
		return fmt.Errorf("unknown exit value %q", string(text))
	}
}

type File string

func (f File) MarshalText() (text []byte, err error) {
	return []byte(f.String()), nil
}

func (f File) String() string {
	return string(f)
}

func (f *File) UnmarshalText(text []byte) error {
	switch File(text) {
	case "", FileAppend, FileDiscard:
		*f = File(text)

		return nil

	default:
		return fmt.Errorf("unknown file value %q", string(text))
	}
}

type Output string

func (o Output) MarshalText() (text []byte, err error) {
	return []byte(o.String()), nil
}

func (o Output) String() string {
	return string(o)
}

func (o *Output) UnmarshalText(text []byte) error {
	switch Output(text) {
	case "", OutputEmpty:
		*o = Output(text)

		return nil

	default:
		return fmt.Errorf("unknown output value %q", string(text))
	}
}

type YAMLCommand Command

func (c *YAMLCommand) UnmarshalYAML(node *yaml.Node) error {
	if node.Kind == yaml.SequenceNode {
		return node.Decode(&c.Command)
	}

	return node.Decode((*Command)(c))
}
