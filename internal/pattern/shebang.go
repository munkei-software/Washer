package pattern

import (
	"bufio"
	"context"
	goerrors "errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"sync"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
)

const TypeShebang = "shebang"

//nolint:gochecknoglobals // Meh.
var firstLineCache sync.Map

func FirstLine(path string) (line string, err error) {
	val, ok := firstLineCache.Load(path)
	if ok {
		if line, ok = val.(string); !ok {
			panic(errors.Internal(nil))
		}

		return
	}

	var f *os.File

	f, err = os.Open(path)
	if err != nil {
		return
	}

	defer func() {
		err = errör.Composite(err, f.Close())
	}()

	reader := bufio.NewReader(f)

	if line, err = reader.ReadString('\n'); err != nil {
		if goerrors.Is(err, io.EOF) {
			err = nil
		} else {
			return
		}
	}

	firstLineCache.Store(path, line)

	return
}

type Shebang struct {
	regex *regexp.Regexp
}

func NewShebang(str string) (*Shebang, error) {
	regex, err := compileRegexp(str)
	if err != nil {
		return nil, err
	}

	return &Shebang{
		regex: regex,
	}, nil
}

func (s *Shebang) Match(_ context.Context, f *file.File) (bool, error) {
	head, err := f.FirstLine()
	if err != nil {
		return false, fmt.Errorf(
			"failed to read file %q when matching pattern: %w",
			f,
			err,
		)
	}

	return strings.HasPrefix(head, "#!") &&
		s.regex.MatchString(head[2:]), nil
}
