package pattern

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/util"
	"gopkg.in/yaml.v3"
)

var (
	_ Matcher                = (*Pattern)(nil)
	_ util.Element[*Pattern] = (*Pattern)(nil)
	_ yaml.Marshaler         = (*Pattern)(nil)
	_ yaml.Unmarshaler       = (*Pattern)(nil)
)

func compileRegexp(str string) (*regexp.Regexp, error) {
	regex, err := regexp.Compile(str)
	if err != nil {
		return nil, fmt.Errorf("invalid regex in pattern %q: %w", str, err)
	}

	return regex, nil
}

type Matcher interface {
	Match(context.Context, *file.File) (bool, error)
}

type Pattern struct {
	implementation Matcher
	Type, String   string
}

func NewPattern(typ, str string) (p *Pattern, err error) {
	p = &Pattern{
		Type:   typ,
		String: str,
	}

	switch p.Type {
	case TypeContent:
		p.implementation, err = NewContent(p.String)

	case "", TypeGlob:
		p.implementation, err = NewGlob(p.String)

	case TypeRegex:
		p.implementation, err = NewRegex(p.String)

	case TypeShebang:
		p.implementation, err = NewShebang(p.String)

	default:
		err = fmt.Errorf(
			"%w: unknown pattern type %q",
			errors.ErrUser,
			p.Type,
		)
	}

	return
}

func (p *Pattern) Compare(other *Pattern) (i int) {
	i = strings.Compare(p.Type, other.Type)

	if i != 0 {
		return
	}

	i = strings.Compare(p.String, other.String)

	return
}

func (p *Pattern) Equals(other *Pattern) bool {
	return p.Type == other.Type && p.String == other.String
}

func (p *Pattern) MarshalYAML() (interface{}, error) {
	if p.Type == "" {
		return p.String, nil
	}

	return map[string]string{
		p.Type: p.String,
	}, nil
}

func (p *Pattern) Match(ctx context.Context, f *file.File) (bool, error) {
	return p.implementation.Match(ctx, f)
}

func (p *Pattern) UnmarshalYAML(value *yaml.Node) error {
	var str string
	if err := value.Decode(&str); err != nil {
		return err
	}

	typ := value.Tag
	if strings.HasPrefix(typ, "!") &&
		!strings.HasPrefix(typ, "!!") {
		typ = typ[1:]
	} else {
		typ = TypeGlob
	}

	tmp, err := NewPattern(typ, str)
	if err != nil {
		return err
	}

	*p = *tmp

	return nil
}
