package pattern_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/pattern"
)

func TestNewGlob(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	for _, tt := range [...]struct {
		Glob      string
		Good, Bad []string
	}{
		{
			Glob: ".*/",
			Good: []string{".hidden/", "dir/.hidden/"},
		},

		{
			Glob: "/*",
			Good: []string{"foo.txt"},
			Bad:  []string{"dir/foo.txt"},
		},

		{
			Glob: "/.*/",
			Good: []string{".hidden/foo.txt"},
			Bad:  []string{"dir/foo.txt", "sub/dir/foo.txt"},
		},

		{
			Glob: "dir/",
			Good: []string{"dir/foo.txt", "sub/dir/foo.txt"},
			Bad:  []string{"foo.txt"},
		},

		{
			Glob: "*",
			Good: []string{"foo.txt", "dir/foo.txt"},
		},
	} {
		tt := tt

		t.Run(tt.Glob, func(t *testing.T) {
			t.Parallel()

			g, err := pattern.NewGlob(tt.Glob)

			require.NoError(t, err)

			for _, s := range [...]struct {
				Paths     []string
				Assertion assert.BoolAssertionFunc
			}{
				{tt.Good, assert.True},
				{tt.Bad, assert.False},
			} {
				for _, path := range s.Paths {
					f, err := file.New("", path)
					require.NoError(t, err)
					ok, err := g.Match(ctx, f)
					s.Assertion(t, ok, "%q ~ %q", tt.Glob, f)
					assert.NoError(t, err)
				}
			}
		})
	}
}
