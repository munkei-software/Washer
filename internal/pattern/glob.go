package pattern

import (
	"context"
	"fmt"
	"strings"

	"github.com/bmatcuk/doublestar/v3"
	"gitlab.com/biffen/washer/internal/file"
)

var _ Matcher = (*Glob)(nil)

const TypeGlob = "glob"

type Glob struct {
	str, glob string
}

func NewGlob(str string) (*Glob, error) {
	glob := str

	if strings.HasPrefix(glob, "/") {
		glob = glob[1:]
	} else {
		glob = "**/" + glob
	}

	if strings.HasSuffix(glob, "/") {
		glob += "**"
	}

	if _, err := doublestar.Match(glob, ""); err != nil {
		return nil, fmt.Errorf("invalid glob in pattern %q: %w", str, err)
	}

	return &Glob{
		str:  str,
		glob: glob,
	}, nil
}

func (g *Glob) Match(_ context.Context, f *file.File) (ok bool, err error) {
	ok, err = doublestar.Match(g.glob, f.Path)
	if err != nil {
		err = fmt.Errorf(
			"failed to match glob %q against file %q: %w",
			g.glob,
			f,
			err,
		)
	}

	return
}
