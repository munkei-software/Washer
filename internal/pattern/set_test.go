package pattern_test

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/pattern"
	"gopkg.in/yaml.v3"
)

var _ pattern.Matcher = MockPattern("")

func TestSortedSet_YAML(t *testing.T) {
	t.Parallel()

	var (
		ctx = context.Background()
		s   pattern.Set
	)

	err := yaml.Unmarshal([]byte(`
- .*/
- '*.exe'
- TAGS
`), &s)
	require.NoError(t, err)

	for _, c := range [...]struct {
		Path string
		Test assert.BoolAssertionFunc
	}{
		{"bar", assert.False},
		{"foo", assert.False},
		{".hidden/file", assert.True},
		{"foobar.exe", assert.True},
	} {
		ok, _, err2 := s.Match(ctx, &file.File{
			Path: "/" + c.Path,
			WD:   "/",
		})
		assert.NoError(t, err2)
		c.Test(t, ok, "path: %q", c.Path)
	}

	y, err := yaml.Marshal(&s)
	assert.NoError(t, err)
	assert.Equal(t, strings.TrimSpace(`
- glob: '*.exe'
- glob: .*/
- glob: TAGS
`), strings.TrimSpace(string(y)))
}

type MockPattern string

//nolint:interfacer // Nah.
func (mp MockPattern) Compare(other MockPattern) int {
	return strings.Compare(mp.String(), other.String())
}

func (mp MockPattern) Match(
	context.Context,
	*file.File,
) (bool, error) {
	return true, nil
}

func (mp MockPattern) String() string {
	return string(mp)
}

func (MockPattern) Type() string {
	return "mock"
}
