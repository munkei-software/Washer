package backup

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"

	errör "gitlab.com/biffen/error"
)

const ErrInvalidIdentifier = Error("invalid backup identifier")

var (
	_ io.Closer    = (*Backup)(nil)
	_ fmt.Stringer = (*Backup)(nil)
)

var _ error = Error("")

func New(_ context.Context, path, identifier string) (b *Backup, err error) {
	if strings.ContainsAny(
		identifier,
		string(os.PathSeparator)+string(os.PathListSeparator),
	) {
		return nil, fmt.Errorf("%w: %q", ErrInvalidIdentifier, identifier)
	}

	var in *os.File

	if in, err = os.Open(path); err != nil {
		if os.IsNotExist(err) {
			err = nil
		} else {
			err = fmt.Errorf(
				"failed to create backup for file %q: failed to read file: %w",
				path,
				err,
			)
		}

		return
	}

	defer func() {
		err = errör.Composite(err, in.Close())
	}()

	var (
		basePath = fmt.Sprintf("%s~%s~", path, identifier)
		backup   = basePath
		i        = 0
		out      *os.File
	)

	for {
		//nolint:gomnd // Oh, shut up!
		out, err = os.OpenFile(backup, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0o600)

		switch {
		case os.IsExist(err):
			i++
			backup = fmt.Sprintf("%s%d~", basePath, i)

			continue

		case err != nil:
			err = fmt.Errorf(
				"failed to create backup file for file %q: %w",
				path,
				err,
			)

			return
		}

		break
	}

	defer func() {
		err = errör.Composite(err, out.Close())
	}()

	if _, err = io.Copy(out, in); err != nil {
		err = fmt.Errorf(
			"failed to create backup for file %q: failed to copy content: %w",
			path,
			err,
		)

		return
	}

	b = &Backup{
		orig:   path,
		backup: backup,
	}

	return
}

type Backup struct {
	orig, backup string
	mtx          sync.Mutex
}

func (b *Backup) Close() error {
	b.mtx.Lock()
	defer b.mtx.Unlock()

	if b.backup == "" {
		return nil
	}

	var (
		err      error
		contents [2][]byte
	)

	for i, path := range [...]string{b.orig, b.backup} {
		contents[i], err = os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("failed to read backup file %q: %w", path, err)
		}
	}

	if bytes.Equal(contents[0], contents[1]) {
		if err := os.Remove(b.backup); err != nil {
			return fmt.Errorf(
				"failed to remove backup of %q (%q): %w",
				b.orig,
				b.backup,
				err,
			)
		}

		b.backup = ""
	}

	return nil
}

func (b *Backup) Original() string {
	return b.orig
}

func (b *Backup) Restore() error {
	b.mtx.Lock()
	defer b.mtx.Unlock()

	if err := os.Rename(b.backup, b.orig); err != nil {
		return fmt.Errorf(
			"failed to restore backup of %q (%q): %w",
			b.orig,
			b.backup,
			err,
		)
	}

	b.backup = ""

	return nil
}

func (b *Backup) String() string {
	b.mtx.Lock()
	defer b.mtx.Unlock()

	return b.backup
}

type Error string

func (e Error) Error() string {
	return string(e)
}
