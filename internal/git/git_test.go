package git_test

import (
	"context"
	"errors"
	"os/exec"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/git"
)

func TestDirectory(t *testing.T) {
	t.Parallel()

	var (
		ctx           = context.Background()
		expected, err = filepath.Abs("../..")
	)

	require.NoError(t, err)

	actual, err := git.Directory(ctx, ".")
	require.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestRepository_Ignored(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	repo, err := git.New(ctx, ".")
	if errors.Is(err, exec.ErrNotFound) {
		t.Skip("no git")
	}

	require.NoError(t, err)

	for _, c := range [...]struct {
		string
		assert.BoolAssertionFunc
	}{
		{"../../.hidden", assert.True},
		{"file", assert.False},
	} {
		ignored := repo.Ignored(ctx, c.string)
		c.BoolAssertionFunc(t, ignored, "%q", c.string)
	}
}
