package internal

import (
	"context"
	"io"

	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
)

type Observer interface {
	io.Closer

	Open(context.Context) error
	BeforeTask(context.Context, *spec.Tool, *file.File) error
	AfterTask(context.Context, *task.Result) error
}
