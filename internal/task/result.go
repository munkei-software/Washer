package task

import (
	"bytes"
	"context"
	"strings"
	"sync"

	"gitlab.com/biffen/washer/internal/exec"
	"gitlab.com/biffen/washer/internal/location"
	"gitlab.com/biffen/washer/internal/parse"
)

type Result struct {
	Task
	exec.Result
	Failure string

	parse  sync.Once
	errors []location.Error
}

func (r *Result) Errors(
	ctx context.Context,
) (errors []location.Error, err error) {
	r.parse.Do(func() {
		if r.Success {
			return
		}

		parser := strings.ToLower(r.Task.Tool.Parse)
		if r.errors, err = parse.Parse(
			ctx,
			parser,
			bytes.NewReader(r.Stdout),
			bytes.NewReader(r.Stderr),
		); err != nil {
			err = nil
		}

		if len(r.errors) == 0 {
			r.errors = []location.Error{
				{
					Message: strings.TrimSpace(
						string(r.Stdout) + "\n" + string(r.Stderr),
					),
				},
			}
		}

		if r.File != nil {
			for i := range r.errors {
				if r.errors[i].Location.File == "" {
					r.errors[i].Location.File = r.File.Path
				}

				if r.errors[i].Location.Line == 0 {
					r.errors[i].Location.Line = 1
				}
			}
		}
	})

	errors = r.errors

	return
}

func (r *Result) Less(other *Result) bool {
	if r.Tool == other.Tool {
		return r.File.Compare(other.File) < 0
	}

	return r.Tool.Compare(other.Tool) < 0
}

func (r *Result) Path() (path string) {
	return r.File.Path
}

func (r *Result) String() string {
	if r.Success {
		return "OK"
	}

	return "Fail"
}
