package task

import (
	"bytes"
	"context"
	"fmt"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/exec"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
)

const (
	OperationCheck Operation = false
	OperationFix   Operation = true
)

var _ fmt.Stringer = (*Task)(nil)

func Make(
	ctx context.Context,
	operation Operation,
	tools *spec.ToolSet,
	files []*file.File,
	out chan<- *Task,
) (err error) {
	eachErr := tools.Each(ctx, func(tool *spec.Tool) bool {
		var command *spec.Command
		switch operation {
		case OperationCheck:
			command = (*spec.Command)(tool.CheckCmd)
		case OperationFix:
			command = (*spec.Command)(tool.FixCmd)
		}

		if command == nil {
			// No command for this tool and operation, that’s OK
			return true
		}

		for _, file := range files {
			file := file
			var ok bool
			ok, err = tool.Includes(ctx, file)
			switch {
			case err != nil:
				return false
			case !ok:
				// Tool doesn’t include _this_ file; let’s try the next file
				continue
			}

			t := New(tool, command, file)

			if command.Global {
				t.File = nil
				out <- t

				// Global tool only needs to match one file
				return true
			}

			out <- t
		}

		return true
	})

	return errör.Composite(err, eachErr)
}

type Operation bool

type Task struct {
	File *file.File
	Tool *spec.Tool

	command *spec.Command
}

func New(
	tool *spec.Tool,
	command *spec.Command,
	f *file.File,
) *Task {
	return &Task{
		File:    f,
		Tool:    tool,
		command: command,
	}
}

func (t *Task) FullCommand() []string {
	if t.File == nil {
		return t.command.Command
	}

	switch t.command.File {
	// TODO explicit append+global → append all files
	case "", spec.FileAppend:
		if t.command.Global {
			return t.command.Command
		}

		return append(t.command.Command, t.File.Path)

	case spec.FileDiscard:
		return t.command.Command

	default:
		panic(errors.Internal(nil))
	}
}

func (t Task) Run(ctx context.Context) (*Result, error) {
	var wd string

	if t.File != nil {
		wd = t.File.WD
	}

	res, err := exec.Exec(ctx, wd, t.FullCommand()...)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to run tool %q with file %q: %w",
			t.Tool,
			t.File,
			err,
		)
	}

	r := &Result{
		Task:   t,
		Result: *res,
	}

	switch t.command.Exit {
	case "":
		r.Success = res.ExitCode == 0
		r.Failure = "non-zero exit"

	case spec.ExitIgnore:
		r.Success = true

	case spec.ExitInverse:
		r.Success = res.ExitCode != 0
		r.Failure = "zero exit"

	default:
		panic(errors.Internal(nil))
	}

	switch t.command.Output {
	case "":

	case spec.OutputEmpty:
		if len(bytes.TrimRight(res.Stdout, "\n")) > 0 {
			r.Success = false
			r.Failure = "non-empty output"
		}

	default:
		panic(errors.Internal(nil))
	}

	if res.Error != nil {
		r.Success = false
		r.Failure = fmt.Sprintf("error (%v)", res.Error)
	}

	return r, nil
}

func (t *Task) String() string {
	if t.File == nil {
		return t.Tool.String()
	}

	return fmt.Sprintf("%s @ %s", t.Tool, t.File)
}
