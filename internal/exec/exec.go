package exec

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"time"

	"gitlab.com/biffen/washer/internal/errors"
)

type Result struct {
	Command        []string
	Error          error
	ExitCode       int
	Stdout, Stderr []byte
	Success        bool
	Time           *time.Duration
}

func Exec(
	ctx context.Context,
	wd string,
	args ...string,
) (res *Result, err error) {
	if len(args) == 0 {
		err = fmt.Errorf("%w: empty command", errors.ErrUser)

		return
	}

	//nolint:gosec // That’s the point!
	cmd := exec.CommandContext(ctx, args[0], args[1:]...)

	cmd.Dir = wd

	var o, e bytes.Buffer
	cmd.Stderr = &e
	cmd.Stdout = &o

	start := time.Now()
	runErr := cmd.Run()
	dur := time.Since(start)

	res = &Result{
		Command: args,
		Stderr:  e.Bytes(),
		Error:   runErr,
		Stdout:  o.Bytes(),
		Time:    &dur,
	}

	if cmd.ProcessState != nil {
		res.Error = nil
		res.ExitCode = cmd.ProcessState.ExitCode()
		res.Success = cmd.ProcessState.Success()
	}

	return
}
