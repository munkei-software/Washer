package exec_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/exec"
)

func TestExec(t *testing.T) {
	t.Parallel()

	for _, tt := range [...]struct {
		Args     []string
		Expected exec.Result
	}{
		{
			Args: []string{"true"},
			Expected: exec.Result{
				Success: true,
			},
		},

		{
			Args: []string{"false"},
			Expected: exec.Result{
				ExitCode: 1,
				Success:  false,
			},
		},

		{
			Args: []string{"printf", "<%s>", "foo"},
			Expected: exec.Result{
				Success: true,
				Stdout:  []byte("<foo>"),
			},
		},
	} {
		tt := tt

		t.Run(fmt.Sprintf("%q", tt.Args), func(t *testing.T) {
			t.Parallel()

			var (
				actual *exec.Result
				ctx    = context.Background()
			)

			require.NotPanics(t, func() {
				var err error
				actual, err = exec.Exec(ctx, ".", tt.Args...)
				assert.NoError(t, err)
			})

			assert.Equal(t, string(tt.Expected.Stderr), string(actual.Stderr))
			assert.Equal(t, tt.Expected.ExitCode, actual.ExitCode)
			assert.Equal(t, string(tt.Expected.Stdout), string(actual.Stdout))
			assert.Equal(t, tt.Expected.Success, actual.Success)
		})
	}
}
