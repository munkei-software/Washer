package parse

import (
	"bufio"
	"context"
	"io"
	"regexp"
	"strings"

	"gitlab.com/biffen/washer/internal/location"
	"gitlab.com/biffen/washer/internal/util"
)

var (
	_ parser = GNU

	//nolint:gochecknoglobals // Meh.
	gnuPatterns = util.Transform(
		context.Background(),
		regexp.MustCompile,
		// file1:line1.column1-file2:line2.column2: message
		gnuProgram+gnuFile+gnuLine+`\.`+gnuColumn+`?-`+gnuFile2+gnuLine2+`\.`+gnuColumn2+`?`+gnuMessage,
		// sourcefile:lineno: message
		// sourcefile:lineno:column: message
		// sourcefile:lineno.column: message
		gnuProgram+gnuFile+gnuLine+`(?:[.:]`+gnuColumn+`)?`+gnuMessage,
		// sourcefile:line1-line2: message
		gnuProgram+gnuFile+gnuLine+`-`+gnuLine2+gnuMessage,
		// sourcefile:line1.column1-line2.column2: message
		gnuProgram+gnuFile+gnuLine+`\.`+gnuColumn+`-`+gnuLine2+`\.`+gnuColumn2+gnuMessage,
		// sourcefile:line1.column1-column2: message
		gnuProgram+gnuFile+gnuLine+`\.`+gnuColumn+`-`+gnuColumn2+gnuMessage,
	)
)

const (
	gnuProgram = `^(?:\w+?:)?`
	gnuFile    = `(?P<file>.*?):`
	gnuFile2   = `(?P<file2>.*?):`
	gnuLine    = `(?P<line>\d+)`
	gnuLine2   = `(?P<line2>\d+)`
	gnuColumn  = `(?P<column>\d+)`
	gnuColumn2 = `(?P<column2>\d+)`
	gnuMessage = `:\s*(?P<message>.*)`
)

func GNU(
	ctx context.Context,
	stdout, stderr io.Reader,
) (errors []location.Error, err error) {
	scanner := bufio.NewScanner(io.MultiReader(stdout, stderr))

OUTER:
	for scanner.Scan() {
		line := scanner.Text()

		for _, pattern := range gnuPatterns {
			if match := pattern.FindStringSubmatch(line); match != nil {
				var e location.Error
				if err = util.UnmarshalRegexp(pattern, match, &e); err != nil {
					return
				}

				if e.Location.File != "" {
					errors = append(errors, e)

					continue OUTER
				}
			}
		}

		if l := len(errors); l > 0 {
			errors[l-1].Message += "\n" + line
		}
	}

	if err = scanner.Err(); err != nil {
		return
	}

	err = util.Each(ctx, errors, func(e *location.Error) bool {
		e.Message = strings.TrimSpace(e.Message)

		return true
	})

	return
}
