package parse_test

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/location"
	"gitlab.com/biffen/washer/internal/parse"
)

func TestGNU_Parse(t *testing.T) {
	t.Parallel()

	var (
		u17 uint64 = 17
		u34 uint64 = 34
	)

	var (
		ctx      = context.Background()
		expected = []location.Error{
			{
				Location: location.Location{
					File: "sourcefile-1",
					Line: 42,
				},
				Message: "message 1\nmessage 1 continued",
			},
			{
				Location: location.Location{
					File:   "sourcefile-2",
					Line:   42,
					Column: &u17,
				},
				Message: "message 2",
			},
			{
				Location: location.Location{
					File:   "sourcefile-3",
					Line:   42,
					Column: &u17,
				},
				Message: "message 3",
			},
			{
				Location: location.Location{
					File:    "sourcefile-4",
					Line:    42,
					Column:  &u17,
					Line2:   84,
					Column2: &u34,
				},
				Message: "message 4\n  indented",
			},
			{
				Location: location.Location{
					File:    "sourcefile-5",
					Line:    42,
					Column:  &u17,
					Column2: &u34,
				},
				Message: "message 5\nwith: colon",
			},
			{
				Location: location.Location{
					File:  "sourcefile-6",
					Line:  42,
					Line2: 84,
				},
				Message: "message 6",
			},
			{
				Location: location.Location{
					File:    "sourcefile-7",
					Line:    42,
					Column:  &u17,
					File2:   "sourcefile-8",
					Line2:   84,
					Column2: &u34,
				},
				Message: "message 7",
			},
		}

		text = `
sourcefile-1:42: message 1
message 1 continued
sourcefile-2:42:17: message 2

sourcefile-3:42.17: message 3
sourcefile-4:42.17-84.34: message 4
  indented
sourcefile-5:42.17-34: message 5
with: colon
sourcefile-6:42-84: message 6
sourcefile-7:42.17-sourcefile-8:84.34: message 7
`
	)

	actual, err := parse.GNU(
		ctx,
		strings.NewReader(text),
		strings.NewReader(""),
	)

	require.NoError(t, err)
	require.NotNil(t, actual)

	assert.Equal(t, expected, actual)
}
