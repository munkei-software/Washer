package parse

import (
	"context"
	"io"

	"gitlab.com/biffen/washer/internal/location"
)

var parsers = map[string]parser{
	"gnu":   GNU,
	"junit": JUnit,
}

func Parse(
	ctx context.Context,
	parser string,
	stdout, stderr io.Reader,
) (errors []location.Error, err error) {
	p, ok := parsers[parser]

	if !ok {
		// TODO log that the parser is unknown
		p = GNU
	}

	if errors, err = p(ctx, stdout, stderr); err != nil {
		return nil, err
	}

	return
}

type parser func(
	ctx context.Context,
	stdout, stderr io.Reader,
) ([]location.Error, error)
