package command

import (
	"context"
	goerrors "errors"
	"fmt"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
)

var _ Command = (*RootCommand)(nil)

type RootCommand struct {
	Help bool `option:"help|h"`

	Washer internal.Washer

	SubCommand Command
}

func NewRootCommand(ctx context.Context, args ...string) (*RootCommand, error) {
	cmd := new(RootCommand)

	parser, err := applause.NewParser(cmd)
	if err != nil {
		panic(errors.Internal(err))
	}

	if err = parser.Add(
		"q-", &cmd.Washer.Verbosity,
		"directory|C", &cmd.Washer.Directory,
		"specification|spec|s", &cmd.Washer.Specification,
		"v+", &cmd.Washer.Verbosity,
	); err != nil {
		panic(errors.Internal(err))
	}

	parser.PassThrough = true

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, goerrors.Join(errors.ErrUser, err)
	}

	var command *string
	command, operands = SubCommand(operands...)

	if command == nil {
		tmp := CheckCommand
		command = &tmp
	}

	switch *command {
	case CheckCommand:
		cmd.SubCommand, err = NewCheck(ctx, parser, operands...)

	case FixCommand:
		cmd.SubCommand, err = NewFix(ctx, parser, operands...)

	case DumpCommand:
		cmd.SubCommand, err = NewDump(ctx, parser, operands...)

	default:
		return nil,
			fmt.Errorf("%w: unknown command %q", errors.ErrUser, *command)
	}

	if err != nil {
		return nil, goerrors.Join(errors.ErrUser, err)
	}

	return cmd, nil
}

func (cmd *RootCommand) Close() error {
	return errör.CloseAll(cmd.SubCommand)
}

func (cmd *RootCommand) Run(ctx context.Context) error {
	ctx = internal.WithWasher(ctx, &cmd.Washer)

	if err := cmd.Washer.ChDir(ctx); err != nil {
		return err
	}

	return cmd.SubCommand.Run(ctx)
}
