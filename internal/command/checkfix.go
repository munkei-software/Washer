package command

import (
	"context"
	"os"
	"strings"

	"github.com/logrusorgru/aurora/v3"
	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/debug"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/progress"
	"gitlab.com/biffen/washer/internal/task"
	"golang.org/x/term"
)

// CheckFix is base for the check and fix sub-commands.
type CheckFix struct {
	Backup   bool               `option:"backup"`
	Dry      bool               `option:"dry-run|n"`
	Git      internal.GitOption `option:"git"`
	Jobs     uint64             `option:"jobs|j"`
	NoTools  []string           `option:"no-tool|T"`
	Paths    []string
	Progress string   `option:"progress|p"`
	Tools    []string `option:"tool|t"`

	hc          internal.HarnessConfig
	setupFormat setupFunc
}

func (cf *CheckFix) Run(ctx context.Context) (err error) {
	hc := cf.hc

	hc.Backup = cf.Backup
	hc.Dry = cf.Dry
	hc.ExplicitFiles = cf.Paths
	hc.GitOption = cf.Git
	hc.Jobs = cf.Jobs

	for _, f := range [...]setupFunc{
		cf.setupFormat,
		cf.setupProgress,
	} {
		if f == nil {
			continue
		}

		if err = f(ctx, &hc); err != nil {
			return
		}
	}

	s, err := internal.GetWasher(ctx).Spec(ctx)
	if err != nil {
		return err
	}

	if len(cf.NoTools) == 0 {
		if noTools := os.Getenv(internal.EnvWasherNoTool); noTools != "" {
			cf.NoTools = strings.Split(noTools, ",")
		}
	}

	hc.Tools, err = s.GetTools(ctx, cf.Tools, cf.NoTools)
	if err != nil {
		return err
	}

	var h *internal.Harness

	if h, err = internal.NewHarness(ctx, &hc); err != nil {
		return
	}

	defer func() {
		err = errör.Composite(err, h.Close())
	}()

	var results []*task.Result

	if results, err = h.Run(ctx); err != nil {
		return
	}

	for _, res := range results {
		if !res.Success {
			err = errors.ErrFailure

			debug.Printf("at least one test failed")

			break
		}
	}

	return
}

func (cf *CheckFix) setupProgress(
	_ context.Context,
	hc *internal.HarnessConfig,
) error {
	p := cf.Progress

	if p == "" {
		if env := os.Getenv(internal.EnvWasherProgress); env == "" {
			p = progress.ProgressCount
		} else {
			p = env
		}
	}

	if term.IsTerminal(int(os.Stderr.Fd())) {
		prog, err := progress.New(p, progress.Config{
			Aurora: aurora.NewAurora(true),
			Writer: os.Stderr,
		})
		if err != nil {
			return err
		}

		if prog != nil {
			hc.Observers = append(hc.Observers, prog)
		}
	}

	return nil
}

type setupFunc func(context.Context, *internal.HarnessConfig) error
