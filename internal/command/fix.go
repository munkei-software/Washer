package command

import (
	"context"

	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/task"
)

var _ Command = (*Fix)(nil)

const (
	FixCommand = "fix"
)

type Fix struct {
	CheckFix
}

func NewFix(
	ctx context.Context,
	parser *applause.Parser,
	args ...string,
) (f *Fix, err error) {
	f = &Fix{
		CheckFix: CheckFix{
			Backup: true,

			hc: internal.HarnessConfig{
				GitStash:  false,
				Lock:      true,
				Operation: task.OperationFix,
			},
		},
	}

	parser.PassThrough = false
	if err = parser.Add(f); err != nil {
		panic(errors.Internal(err))
	}

	f.Paths, err = parser.Parse(ctx, args)
	if err != nil {
		return
	}

	return
}

func (fix *Fix) Close() error {
	return nil
}
