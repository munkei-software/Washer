package format

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/junit"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
)

var _ Format = (*JUnit)(nil)

const FormatJUnit = "junit"

type JUnit struct {
	Config

	mtx        sync.Mutex
	start      time.Time
	testSuites junit.TestSuites
}

func NewJUnit(config Config) *JUnit {
	return &JUnit{
		Config: config,
		testSuites: junit.TestSuites{
			Name: "washer",
		},
	}
}

func (j *JUnit) AfterTask(_ context.Context, res *task.Result) error {
	j.mtx.Lock()
	defer j.mtx.Unlock()

	testCase := &junit.TestCase{
		Classname: res.Tool.String(),
		Name:      res.Task.String(),
		SystemErr: res.Stderr,
		SystemOut: res.Stdout,
		Time:      junit.NewDuration(res.Time),
	}

	if res.File != nil {
		testCase.File = res.File.Path
	}

	message := fmt.Sprintf("%s failed", res.FullCommand())

	if res.Error != nil {
		testCase.Error = &junit.Error{
			Message:     message,
			Description: res.Error.Error(),
		}
	}

	if !res.Success {
		var description string
		if res.File == nil {
			description = fmt.Sprintf("Tool %s failed", res.Tool)
		} else {
			description = fmt.Sprintf("Tool %s failed for file %s", res.Tool, res.File)
		}

		testCase.Failure = &junit.Failure{
			Message:     message,
			Description: description,
		}
	}

	suite := j.testSuites.Suite(res.Tool.Name, time.Now())

	suite.TestCases = append(suite.TestCases, testCase)

	return nil
}

func (j *JUnit) BeforeTask(context.Context, *spec.Tool, *file.File) error {
	return nil
}

func (j *JUnit) Close() (err error) {
	t := time.Since(j.start)
	j.testSuites.Time = junit.NewDuration(&t)

	if err = j.testSuites.Print(j.Writer); err != nil {
		return
	}

	_, err = fmt.Fprintln(j.Writer, "")

	return
}

func (j *JUnit) Open(context.Context) error {
	j.start = time.Now()

	return nil
}
