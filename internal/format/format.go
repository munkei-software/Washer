package format

import (
	"fmt"
	"io"

	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
)

const (
	FormatNone  = "none"
	indentation = "  "
)

type Config struct {
	ASCII     bool
	Colour    bool
	Verbosity int
	Writer    io.Writer
}

type Format interface {
	internal.Observer
}

func New(format string, config Config) (Format, error) {
	switch format {
	case "", FormatText:
		return NewText(config), nil

	case FormatCodeclimate:
		return NewCodeclimate(config), nil

	case FormatJUnit:
		return NewJUnit(config), nil

	case FormatNone:
		return nil, nil

	default:
		return nil,
			fmt.Errorf("%w: unknown format %q", errors.ErrUser, format)
	}
}
