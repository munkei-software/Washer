package internal

import (
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/backup"
	"gitlab.com/biffen/washer/internal/debug"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/git"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
	"gitlab.com/biffen/washer/internal/util"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"
)

const (
	GitOptionAll     GitOption = "all"
	GitOptionChanged GitOption = "changed"
	GitOptionStaged  GitOption = "staged"
	GitOptionTracked GitOption = "tracked"
)

var (
	_ flag.Value = (*GitOption)(nil)
	_ io.Closer  = (*Harness)(nil)
)

func DefaultPath(f *file.File) util.Transformer[*file.File] {
	return func(
		ctx context.Context,
		in <-chan *file.File,
		out chan<- *file.File,
	) error {
		var count uint64

		for f := range in {
			out <- f
			count++
		}

		if count == 0 {
			debug.Printf("No paths specified; defaulting to %q", f)

			out <- f
		}

		return nil
	}
}

func gitFiles(
	f func(context.Context, []string) ([]string, error),
) util.Transformer[*file.File] {
	return func(ctx context.Context, in <-chan *file.File, out chan<- *file.File) error {
		var paths []string

		for f := range in {
			paths = append(paths, f.Path)
		}

		paths, err := f(ctx, paths)
		if err != nil {
			return fmt.Errorf("failed to list Git files: %w", err)
		}

		debug.Printf("Git reports %d files", len(paths))

		for _, path := range paths {
			f, err := file.New("", path)
			if err != nil {
				return err
			}

			if f.IsDir() {
				// Git doesn’t ordinarily list directories, so this is
				// _probably_ a submodule. Skip it.
				continue
			}

			out <- f
		}

		return nil
	}
}

func quote(str string) string {
	for _, r := range str {
		if !strings.ContainsRune(
			"+,-./"+
				"0123456789"+
				":@"+
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ"+
				"]^_"+
				"abcdefghijklmnopqrstuvwxyz"+
				"{}",
			r,
		) {
			return "'" + strings.ReplaceAll(str, "'", `'"'"'`) + "'"
		}
	}

	return str
}

func removeIgnored(repo *git.Repository) util.Transformer[*file.File] {
	return func(ctx context.Context, in <-chan *file.File, out chan<- *file.File) error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()

			case f, ok := <-in:
				if !ok {
					return nil
				}

				if repo.Ignored(ctx, f.Path) {
					continue
				}

				out <- f
			}
		}
	}
}

type GitOption string

func (g *GitOption) Set(str string) error {
	switch GitOption(str) {
	case GitOptionAll,
		GitOptionChanged,
		GitOptionStaged,
		GitOptionTracked:
		*g = GitOption(str)

		return nil

	default:
		return fmt.Errorf("unknown Git option %q", str)
	}
}

func (g GitOption) String() string {
	return string(g)
}

type Harness struct {
	HarnessConfig

	backups      map[string]*backup.Backup
	fileLocks    map[string]*sync.Mutex
	backupsMtx   sync.Mutex
	fileLocksMtx sync.Mutex
	tasksQueue   chan *task.Task
}

func NewHarness(
	ctx context.Context,
	config *HarnessConfig,
) (h *Harness, err error) {
	h = &Harness{
		HarnessConfig: *config,

		backups:   make(map[string]*backup.Backup),
		fileLocks: make(map[string]*sync.Mutex),
	}

	if h.GitStash && !h.Dry && h.GitOption == GitOptionStaged {
		dir := GetWasher(ctx).Directory

		repo, err := git.New(ctx, dir)
		if err != nil {
			return nil, fmt.Errorf(
				"failed to work out Git repository for %q: %w",
				dir,
				err,
			)
		}

		h.Observers = append(h.Observers, &GitIndex{
			repo: *repo,
		})
	}

	return
}

func (h *Harness) Close() (err error) {
	err = h.eachObserver(func(observer Observer) error {
		return observer.Close()
	})

	if h.Backup {
		h.backupsMtx.Lock()
		defer h.backupsMtx.Unlock()

		err = errör.CloseAll(util.Transform(
			context.Background(),
			func(b *backup.Backup) io.Closer {
				return b
			},
			maps.Values(h.backups)...,
		)...)
	}

	return
}

func (h *Harness) Run(ctx context.Context) (results []*task.Result, err error) {
	if err = h.eachObserver(func(observer Observer) error {
		return observer.Open(ctx)
	}); err != nil {
		return
	}

	if h.Jobs == 0 {
		h.Jobs = uint64(runtime.NumCPU())
		if h.Jobs < 1 {
			h.Jobs = 1
		}
	}

	w := GetWasher(ctx)

	debug.Printf("%d jobs", h.Jobs)

	transformers, err := w.GetFiles(ctx, h.GitOption)
	if err != nil {
		return nil, err
	}

	if len(h.Exclude) > 0 {
		transformers = append(transformers,
			util.Exclude(func(f *file.File) (exclude bool) {
				abs := f.Abs()

				for _, path := range h.Exclude {
					if exclude = abs == path; exclude {
						debug.Printf("excluding %q", path)
					}
				}

				return
			}),
		)
	}

	g, ctx := errgroup.WithContext(ctx)

	sc := util.Chain[*file.File]{
		ChanSize:     int(h.Jobs),
		Transformers: transformers,
	}

	var (
		tmp []*file.File
		wd  = GetWasher(ctx).Directory
	)

	if tmp, err = util.TransformErr(ctx, func(str string) (*file.File, error) {
		return file.New(wd, str)
	}, h.ExplicitFiles...); err != nil {
		return
	}

	explicitFiles := util.NewSortedSet(ctx, tmp...)

	if err2 := explicitFiles.Each(ctx, func(f *file.File) bool {
		if exists, _ := f.Exists(); !exists {
			err = fmt.Errorf("%w: no such file %q", errors.ErrUser, f)
		}

		return err == nil
	}); err == nil && err2 != nil {
		err = err2
	}

	if err != nil {
		return
	}

	files, f := sc.Run(ctx, explicitFiles.Slice()...)

	g.Go(f)

	h.tasksQueue = make(chan *task.Task, h.Jobs)

	g.Go(func() error {
		return h.listTests(ctx, files)
	})

	resultsQueue := make(chan *task.Result, h.Jobs+h.Jobs)
	h.work(ctx, resultsQueue, g)

	g.Go(func() error {
		return h.readResults(ctx, resultsQueue, &results)
	})

	err = g.Wait()

	return
}

func (h *Harness) backup(ctx context.Context, f *file.File) error {
	if f == nil || !h.Backup {
		return nil
	}

	path := f.Abs()

	h.backupsMtx.Lock()
	defer h.backupsMtx.Unlock()

	if _, ok := h.backups[path]; ok {
		return nil
	}

	b, err := backup.New(ctx, path, name)
	if err != nil {
		return fmt.Errorf("failed to backup file %q: %w", f, err)
	}

	if b != nil {
		h.backups[path] = b
	}

	return nil
}

func (h *Harness) eachObserver(f func(Observer) error) (err error) {
	for _, observer := range h.Observers {
		err = errör.Composite(err, f(observer))
	}

	return
}

func (h *Harness) fileLock(f *file.File) sync.Locker {
	if f == nil || !h.Lock {
		return nil
	}

	key := f.Abs()

	h.fileLocksMtx.Lock()
	defer h.fileLocksMtx.Unlock()

	mtx, ok := h.fileLocks[key]
	if !ok {
		mtx = new(sync.Mutex)
		h.fileLocks[key] = mtx
	}

	return mtx
}

func (h *Harness) listTests(
	ctx context.Context,
	queue <-chan *file.File,
) error {
	defer close(h.tasksQueue)

	debug.Printf("START listing tests")

	files, err := util.ChanToSlice(ctx, queue)
	if err != nil {
		return err
	}

	err = task.Make(ctx, h.Operation, h.Tools, files, h.tasksQueue)

	debug.Printf("DONE listing tests")

	return err
}

func (h *Harness) readResults(
	ctx context.Context,
	resultsQueue <-chan *task.Result,
	results *[]*task.Result,
) error {
	debug.Printf("START reading results")

	for {
		select {
		case <-ctx.Done():
			debug.Printf("CANCELLED reading results")

			return ctx.Err()

		case res, ok := <-resultsQueue:
			if !ok {
				debug.Printf("DONE reading results")

				return nil
			}

			if err := h.eachObserver(func(observer Observer) error {
				return observer.AfterTask(ctx, res)
			}); err != nil {
				return err
			}

			*results = append(*results, res)
		}
	}
}

func (h *Harness) work(
	ctx context.Context,
	resultsQueue chan<- *task.Result,
	g *errgroup.Group,
) {
	var wg sync.WaitGroup

	for i := uint64(0); i < h.Jobs; i++ {
		i := i

		wg.Add(1)

		g.Go(func() error {
			defer wg.Done()

			return h.worker(ctx, i, resultsQueue)
		})
	}

	go func() {
		defer close(resultsQueue)

		debug.Printf("START waiting for workers")
		defer debug.Printf("DONE waiting for workers")

		wg.Wait()
	}()
}

func (h *Harness) worker(
	ctx context.Context,
	id uint64,
	resultsQueue chan<- *task.Result,
) error {
	debug.Printf("START worker %d", id)

	type execFunc func(*task.Task) (*task.Result, error)

	var e execFunc

	if h.Dry {
		ps4 := os.Getenv("PS4")
		if ps4 == "" {
			ps4 = "+ "
		}

		e = func(t *task.Task) (*task.Result, error) {
			GetWasher(
				ctx,
			).Logf("%s%s", ps4, strings.Join(util.Transform(ctx, quote, t.FullCommand()...), " "))

			return nil, nil
		}
	} else {
		e = func(t *task.Task) (*task.Result, error) {
			if err := h.backup(ctx, t.File); err != nil {
				return nil, err
			}

			if err := h.eachObserver(func(observer Observer) error {
				return observer.BeforeTask(ctx, t.Tool, t.File)
			}); err != nil {
				return nil, err
			}

			return t.Run(ctx)
		}
	}

	if h.Lock {
		e = func(e execFunc) execFunc {
			return func(t *task.Task) (*task.Result, error) {
				mtx := h.fileLock(t.File)
				if mtx != nil {
					debug.Printf("worker %d: locking file %q", id, t.File)
					defer debug.Printf(
						"worker %d: unlocked file %q",
						id,
						t.File,
					)

					mtx.Lock()
					defer mtx.Unlock()
				}

				return e(t)
			}
		}(e)
	}

	for {
		select {
		case <-ctx.Done():
			debug.Printf("CANCELLED worker %d", id)

			return ctx.Err()

		case t, ok := <-h.tasksQueue:
			if !ok {
				debug.Printf("DONE worker %d", id)

				return nil
			}

			res, err := e(t)

			switch {
			case err != nil:
				return err

			case res == nil:
				continue
			}

			if res.Success {
				debug.Printf("task %q successful", t)
			} else {
				debug.Printf("task %q failed (%s)", t, res.Failure)
			}

			resultsQueue <- res
		}
	}
}

//nolint:maligned // Not used that much.
type HarnessConfig struct {
	Backup        bool
	Dry           bool
	Exclude       []string
	ExplicitFiles []string
	GitOption     GitOption
	GitStash      bool
	Jobs          uint64
	Lock          bool
	Observers     []Observer
	Operation     task.Operation
	Tools         *spec.ToolSet
}
