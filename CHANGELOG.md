# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic
Versioning].

## 0.2.3 - 2023-11-27

### Fixed

-   `codeclimate` output now includes fingerprints

## 0.2.2 - 2023-10-11

### Fixed

-   Some Git-related bugs

## 0.2.1 - 2023-05-02

### Fixed

-   Fatal error when running in a Git repository with checked-out submodule(s)

## 0.2.0 - 2022-11-14

### Added

-   `command.exit`
-   `command.file`
-   `command.global`
-   `command.output`

## 0.1.2 - 2022-09-12

### Added

-   Some things can now be configured by environment variables

## 0.1.1 - 2022-09-12

### Fixed

-   Empty Code Climate output is now an array

## 0.1.0 - 2022-05-08

### Added

-   Code Climate output
-   Parse tools’ output (GNU-style and JUnit for now)

### Changed

-   Removed the `--format` option and made the `--output` option take an
    (optional) format as well as be repeatable for multiple outputs. See
    _Usage_

## 0.0.2 - 2022-05-01

Miscellaneous internal fixes.

## 0.0.1 - 2022-01-14

Initial version.

## 0.0.0

Initial non-version.

[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
